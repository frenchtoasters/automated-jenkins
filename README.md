# Jenkins Deployment Automation

This repo contains all the infrasturcture code and images to deploy a Jenkins server using the following tools:

```
git
packer
awscli
terraform
ansible
```

## Update Build image

The process to update the image and push it to the registry is a follows:

* First you need to clone the repository

* Next you need to login to the docker registery for the project, found on the registry tab

* Once logged in you can edit the `Dockerfile/Dockerfile` in the repository and make changes to the image build

* After you have saved the images you need to run the following to build and push the image to the repo

```
> cd DockerFile
> docker build -t registry.gitlab.com/frenchtoasters/automated-jenkins .
> docker push registry.gitlab.com/frenchtoasters/automated-jenkins:latest
```

## Ansible 

The ansible code found in `/Dockerfile/packer/ansible-playbook.yml` will execute the following on all hosts:

* Install the following repositories and GPG keys:

```
Docker-ce
Jenkins
```

* Once repositories are installed it will install the following tools via `apt-get`:

```
docker-ce
python-pip
docker-py
jenkins
```

* Set the following user permissions:

```
usermod -aG docker ubuntu
usermod -aG docker jenkins
```

* Finally it will start the `sonatype/nexus3` container exposing port `8081`

## Packer

The packer code found in `/Dockerfile/config` will execute the following and build out ami:

* First it adds the `ppa:ansible/ansible` repo to the build machine and install ansible

* Once installed it will create `/home/ubuntu/Jenkins` and set the owner as `ubuntu`

* Then it copies the contents of our `/Dockerfile/packer/Jenkins` folder into `/home/ubuntu/Jenkins`

* Finially it runs our `ansible-playbook.yml` and executes the above steps

## Terraform

The terraform code found in `/Dockerfile/terraform` will build out the following AWS infrastructure for us:

```
aws_launch_configuration
aws_autoscaling_group
aws_security_group (jenkins_app_websg, elbsg)
aws_elb
```

The image that will be used for this instance will be the one that was build using our packer code above. Once deployed we are ready to start configuring Jenkins!


## Operation of Repo

This section is going to you show you how to manage jenkins using this repository.

### Building

To create a new instance of Jenkins from this repo you need to first configure your `AWS_KEY` and `AWS_SECRET` in the `CI/CD -> Environment variables` section. Once complete you need to execute the following:

* First clone repo locally

* Then you will need to run `git checkout -b build` to create the `build` branch

* Finally you just need to run the following to first trigger the `Packer_build` stage then the `Terraform_build` stage in the `.gitlab-ci.yml` pipeline and thus building Jenkins

```
git add .
git commit -m "initial build"
git push origin build
```

### Updating

To update the build that you have running you need to execute the following:

* First create the `update` branch `git checkout -b update` 

* Next you will need to make the change you wish to the `terraform` deployment

* Then like building all you need to do to execute this stage is push to the `update` branch like so:

```
git add .
git commit -m "updating build"
git push origin update
```

### Destroying 

To destory the Jenkins install you need to execute the following:

* First create the `destory` branch `git checkout -b destory`

* Next you will need to run the following:

```
git add .
git commit -m "destroying build"
git push origin destory
```

# Demo
[![automate-jenkins-demo](https://asciinema.org/a/jofFbtNJQn4fNiioYY5WuJxhJ.svg)](https://asciinema.org/a/jofFbtNJQn4fNiioYY5WuJxhJ?autoplay=1&speed=2)

# Known issues

* Once configured the `jenkins` service must be restarted before web interface responds, the only error message found is this:

```
Jan 23, 2019 11:21:51 PM hudson.ExtensionFinder$GuiceFinder$FaultTolerantScope$1 error
```

# Legacy

## Update Jenkins image

The process to update the jenkins image is as follows:

* First clone the repository
	
* Next you need to login to the docker registry for the project, found on the registry tab

* Once logged in you can edit the `Dockerfile/packer/Jenkins/Dockerfile` in the repository to make changes to the image build

* To add additional plugins you need to update the `Dockerfile/packer/Jenkins/plugins.txt` with the full ID of the plugin. Id's can be found on `https://plguins.jenkins/io`

* After you have make your changes and saved them you need to run the following to update the jenkins image:

```
> cd DockerFile/packer/Jenkins
> docker build -t registry.gitlab.com/frenchtoasters/automated-jenkins:jenkins .
> docker push registry.gitlab.com/frenchtoasters/automated-jenkins:jenkins
```

