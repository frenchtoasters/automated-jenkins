variable "access_key" {}
variable "secret_key" {}
variable "aws_key_pair" {
  type="map"
}
variable "region" {
  default = "us-east-1"
}
variable "aws_amis" {
  type = "map"
}
variable "aws_vpc" {
  type = "map"
}
variable "ssh_key_path" {
  type = "map"
}

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "us-east-1"
}

data "aws_ami" "jenkins_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["packer-example*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["840324025722"]
}

resource "aws_launch_configuration" "jenkins_app_lc" {
  image_id      = "${lookup(var.aws_amis, var.region)}"
  instance_type = "t2.medium"
  security_groups = ["${lookup(var.aws_vpc, var.region)}"]
  key_name = "${lookup(var.aws_key_pair, var.region)}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "jenkins_app_asg" {
  name                 = "terraform-asg-node-app-${aws_launch_configuration.jenkins_app_lc.name}"
  launch_configuration = "${aws_launch_configuration.jenkins_app_lc.name}"
  availability_zones = ["us-east-1a","us-east-1b"]
  min_size             = 1
  max_size             = 2

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "jenkins_app_websg" {
  name = "security_group_for_jenkins_app_websg"
  ingress {
    from_port = 32769
    to_port = 32769
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "elbsg" {
  name = "security_group_for_elb"
  ingress {
    from_port = 32769
    to_port = 32769
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_availability_zones" "allzones" {}

resource "aws_elb" "elb1" {
  name = "terraform-elb-jenkins-app"
  availability_zones = ["us-east-1a","us-east-1b"]
  security_groups = ["${lookup(var.aws_vpc, var.region)}"]
  
  listener {
    instance_port = 32769
    instance_protocol = "http"
    lb_port = 32769
    lb_protocol = "http"
  }

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "HTTP:32769/"
    interval = 30
  }

  cross_zone_load_balancing = true
  idle_timeout = 400
  connection_draining = true
  connection_draining_timeout = 400

  tags {
    Name = "terraform - elb - jenkins-app"
  }
}
