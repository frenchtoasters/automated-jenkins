access_key = "AWS_ACCESS_KEY"
secret_key = "AWS_SECRET_KEY"
aws_amis = {
  "us-east-1" = "BUILD_IMAGE"
}
aws_vpc = {
  "us-east-1" = "sg-45e54e31"
}
aws_key_pair = {
  "us-east-1" = "minitoast"
}
ssh_key_path = {
  "us-east-1" = "/tmp/terraform/minitoast.pem"
}
